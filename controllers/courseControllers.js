const Course = require("./../models/Course");
const User = require("./../models/User");

module.exports.getAllActive = () => {

	return Course.find({isActive:true}).then(result => {
		return result
	})
}

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
} 

module.exports.getAllCourses = () => {

	return Course.find().then(result => {
		return result
	})
}


//make a route to get single course
module.exports.getSingleCourse = (params) => {

	return Course.findById(params.courseId).then (course => {
		return course
	})
}

//make a route to update the course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//make a route to archive the course
	//meaning, update the status of the course from true to false
module.exports.archiveCourse = (params) => {
	
	let updatedActiveCourse = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//make a route to unarchive the course
	//meaning, update the status of the course from false to true
module.exports.unarchiveCourse = (params) => {
	
	let updatedActiveCourse = {
		isActive: true
	}

	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//make a route to delete a course
	//meaning, the course object will be deleted from the database
module.exports.deleteCourse = (params) => {
	
	return Course.findByIdAndDelete(params).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}